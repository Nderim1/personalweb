angular.module('web').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	 $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

 	$stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/card.template.html',
        controller: 'WebCtrl'

      })

      .state('portfolio', {
        url: '/portfoglio',
        templateUrl: 'app/portfolio/portfolio.template.html'
      })
});